/*
 * @file NT_Pid.h
 * @author Nicolas Toupin
 * @date 2019-11-24
 * @brief Pid function declarations.
 */

#ifndef NT_Pid_h

#include "Arduino.h"

#define TRUE 1
#define FALSE 0

#define NT_Pid_h

class Pid_Values_t
{
public:
	float P_Value = 0; // Proportionnal value
	float I_Value = 0; // Integration value
	float D_Value = 0; // Derivative value
	float Setpoint = 0;
	float Saturation_Error = 0;
	float Process_Variable = 0;
	float Controller_Output = 0;
	float Error = 0;
	float Error_Integral = 0;
	float Error_Derivative = 0;
	long Timestamp = 0;
	long Timestamp_Delta = 0;
};

class Pid_t
{
public:
	Pid_t();
	bool Enable = FALSE;
	float Kp = 0; // Proportionnal gain
	float Ti = 0; // Integration time (second/repetition)
	float Td = 0; // Derivation time (s)
	float PV_Filter = 0; // Time constant of the process variable filter
	float C_Gain = 0; // Saturation error correction gain (anti reset windup)
	float I_Maximum = 0; // Limits the output of the integrator when saturated
	float D_Filter = 0; // Time constant for the derivative filter
	float CO_Minimum = 0;
	float CO_Maximum = 0;
	void Configure(float Kp, float Ki, float Kd);
	void Init();
	void Deinit();
	float Compute(float Setpoint, float Process_Variable);

private:
	Pid_Values_t _Pid_Values;
};

#endif