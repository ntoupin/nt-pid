/*
 * @file Pid.ino
 * @author Nicolas Toupin
 * @date 2019-11-24
 * @brief Simple example of pid utilisation.
 */

#include <NT_Pid.h>

Pid_t Pid1;

void setup()
{
  Pid1.Configure(1, 0, 0);
  Pid1.Init();
}

void loop()
{
  float Setpoint = 10;
  float Process_Variable;
  
  Pid1.Compute(Setpoint, Process_Variable);
}
