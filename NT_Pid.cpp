/*
 * @file NT_Pid.cpp
 * @author Nicolas Toupin
 * @date 2019-11-24
 * @brief Pid function definitions.
 */

#include "Arduino.h"
#include "NT_Pid.h"

Pid_t::Pid_t()
{
}

void Pid_t::Configure(float Kp, float Ki, float Kd)
{
	Kp = Kp;
	Ki = Ki;
	Kd = Kd;
}

void Pid_t::Init()
{
	Enable = TRUE;
}

void Pid_t::Deinit()
{
	Enable = FALSE;
}

// This function uses the ISA or ideal PID algorithm
float Pid_t::Compute(float Setpoint, float Process_Variable)
{
	Pid_Values_t Pid_Actual;

	if (Enable)
	{
		// Time delta since last execution
		Pid_Actual.Timestamp_Delta = micros() - _Pid_Values.Timestamp;

		// Error
		_Pid_Values.Setpoint = Setpoint;
		_Pid_Values.Process_Variable = Process_Variable;
		Pid_Actual.Error = _Pid_Values.Setpoint - _Pid_Values.Process_Variable;

		// Proportionnal
		Pid_Actual.P_Value = Kp * Pid_Actual.Error;

		// Integral
		Pid_Actual.Error_Integral = Pid_Actual.Error * Pid_Actual.Timestamp_Delta; // Integral of error
		Pid_Actual.I_Value = Kp * Ti * Pid_Actual.Error_Integral;

		// Derivative
		Pid_Actual.Error_Derivative = Pid_Actual.Error / Pid_Actual.Timestamp_Delta; // Derivative of error
		Pid_Actual.D_Value = Kp * Td * Pid_Actual.Error_Derivative;

		// Calculate controller output
		Pid_Actual.Controller_Output = Pid_Actual.P_Value + Pid_Actual.I_Value + Pid_Actual.D_Value;
	}
	else
	{
		Pid_Actual.Controller_Output = 0;
	}

	// Save current state
	Pid_Actual.Timestamp = micros();
	_Pid_Values = Pid_Actual;

	return _Pid_Values.Controller_Output;
}